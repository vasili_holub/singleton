﻿namespace Singleton
{
    public class MySingleton
    {
        protected MySingleton()
        {
                
        }

        protected static MySingleton Instance;
        
        public static MySingleton GetInstance()
        {
            if (Instance != null) return Instance;
            Instance = new MySingleton();
            return Instance;
        }
    }
}
