﻿using System;

namespace Singleton
{
    using Singleton;

    class Program
    {
        static void Main(string[] args)
        {
            var mySingleton1 = MySingleton.GetInstance();
            var mySingleton2 = MySingleton.GetInstance();

            ConfigurationApp configurationApp1 = new ConfigurationApp
            {
                SomeParam = "test"
            };

            ConfigurationApp configurationApp2 = new ConfigurationApp() ;

            var inheritedSingleton = configurationApp2.SomeParam == configurationApp1.SomeParam ? "well" : "bad";

            Console.WriteLine($"Singleton inherited works {inheritedSingleton}.");

            var respond = mySingleton1.Equals(mySingleton2) ? "works" : "doesn't work";
            Console.WriteLine($"Singleton design pattern {respond}.");
        }
    }
}
